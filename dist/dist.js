/**
 * initScript copied from inspinia v.2.3
 *
 */

$(document).ready(function() {
  $('body').addClass('fixed-sidebar');
  // Full height
  function fix_height() {
    var heightWithoutNavbar = $("body > #wrapper").height() - 61;
    $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

    var navbarHeigh = $('nav.navbar-default').height();
    var wrapperHeigh = $('#page-wrapper').height();

    if (navbarHeigh > wrapperHeigh) {
      $('#page-wrapper').css("min-height", navbarHeigh + "px");
    }

    if (navbarHeigh < wrapperHeigh) {
      $('#page-wrapper').css("min-height", $(window).height() + "px");
    }

    if ($('body').hasClass('fixed-nav')) {
      if (navbarHeigh > wrapperHeigh) {
        $('#page-wrapper').css("min-height", navbarHeigh - 60 + "px");
      } else {
        $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
      }
    }
  }

  $(window).bind("load resize scroll", function() {
    if (!$("body").hasClass('body-small')) {
      fix_height();
    }
  })

  setTimeout(function() {
    fix_height();
  })
});

// Minimalize menu when screen is less than 768px
$(function() {
  $(window).bind("load resize", function() {
    if ($(this).width() < 769) {
      $('body').addClass('body-small')
    } else {
      $('body').removeClass('body-small')
    }
  })
});
(function() {
  'use strict';

  angular
    .module('zpUtils', []);
})();
(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('paging', paging);

  /** @ngInject */
  function paging() {
    return {
      restrict: 'E',
      templateUrl: 'components/pagination/pagination.html',
      controller: PaginationController,
      controllerAs: 'vm',
      scope: {
        loadingFunction: "&",
        entities: "=",
        onValueChange: "="
      }
    };

    /** @ngInject */
    function PaginationController($scope) {
      var vm = this;

      function range(max) {
        var x = [];
        for (var i = 1; i <= max; i++) {
          x.push(i);
        }
        return x
      }

      function loadPage(page, force) {
        var force = force || false;
        if (page == vm.currentPage && !force) return;
        $scope.loadingFunction()(Number(page)).then(function(response) {
          vm.pagesCount = response.pagesCount || response.totalPages;
          $scope.entities = response.entities;
          vm.pages = range(vm.pagesCount);
          vm.currentPage = page;
        })
      }

      function nextPage() {
        if (vm.currentPage < vm.pagesCount) {
          loadPage(vm.currentPage + 1)
        }
      }

      function previousPage() {
        if (vm.currentPage > 1) {
          loadPage(vm.currentPage - 1)
        }
      }

      loadPage(1);

      vm.showPage = loadPage;
      vm.nextPage = nextPage;
      vm.previousPage = previousPage;


      $scope.$watch('onValueChange', function() {
        loadPage(1, true)
      });

    }
  }

})();

(function() {
  'use strict';

  angular
    .module('zpUtils')
    .factory('zpForms', zpForms);

  /** @ngInject */
  /*TODO: it has to be more professional*/
  function zpForms() {
    function unsubmitForm(form) {
      form.$setPristine();
      form.$setUntouched();
    }

    function clearInputs(form, exp) {
      var patt = new RegExp(exp);
      for (var i in form) {
        if (!patt.test(i)) {
          continue;
        }
        var field = form[i];
        field.$setPristine();
        field.$setUntouched();
      }
    }
    return {
      clearForm: function(form) {
        var formHtml = document.querySelector('form[name="' + form.$name + '"]');
        form.$setPristine();
        form.$setUntouched();
        for (var i in form) {
          if (i.indexOf('$') !== -1) {
            continue;
          }
          var field = form[i];
          var fieldHtml = formHtml.querySelector('[name="' + i + '"]');

          fieldHtml.value = "";
          field.$setUntouched();
        }
        return {};
      },
      resetInputsAndForm: function(form, exp) {
        clearInputs(form, exp);
        unsubmitForm(form);
      },
      unsubmitForm: function(form) {
        if (typeof form === 'undefined') {
          return;
        }
        unsubmitForm(form);
      }
    };
  }

})();
(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('zpBreadcrumb', zpBreadcrumb);

  /** @ngInject */
  function zpBreadcrumb() {
    var directive = {
      restrict: 'E',
      templateUrl: 'components/zp-breadcrumb/zpBreadcrumb.html',
      controller: zpBreadcrumbController,
      controllerAs: 'breadcrumb',
      scope: {}

    };

    return directive;

    /** @ngInject */
    function zpBreadcrumbController($rootScope, $state) {
      var breadcrumb = this;

      function updateBreadcrumb(toState) {
        breadcrumb.path = [];
        toState.data = toState.data || {};
        var statePath = toState.data.breadcrumbPath || [];
        for (var x = 0; x < statePath.length; x++) {
          var stateItem = $state.get(statePath[x]);
          breadcrumb.path.push({
            name: stateItem.data.pageTitle,
            state: stateItem.name
          });
        }
        breadcrumb.currentState = toState.data.pageTitle || "Zaszczepiamy Pasje";
      }

      updateBreadcrumb($state.current);
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
        updateBreadcrumb(toState);
      })

    }

  }

})();
(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('zpMessages', zpMessages);

  /** @ngInject */
  function zpMessages() {
    var directive = {
      restrict: 'A',
      require: '^?form',
      scope: {
        name: "@messageName"
      },

      templateUrl: 'components/zp-messages/zpMessages.html',

      link: zpMessagesLink
    };

    return directive;

    /** @ngInject */
    function zpMessagesLink(scope, element, attr, form) {
      //scope.name = attr.messageName;
      scope.messagesContext = messagesContext;
      scope.showTips = showTips;

      function messagesContext(fieldName) {
        return form[fieldName].$error;
      }

      function showTips(fieldName) {
        return form.$submitted && Object.keys(form[fieldName].$error).length;
      }
    }
  }
})();
(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('minimalizaSidebar', minimalizaSidebar);

  /** @ngInject */
  function minimalizaSidebar($timeout) {
    var directive = {
      restrict: 'A',
      template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
      controller: minimalizaSidebarController
    };
    return directive;

    /** @ngInject */
    function minimalizaSidebarController($scope, $element) {
      $scope.minimalize = function() {
        angular.element('body').toggleClass('mini-navbar');
        if (!angular.element('body').hasClass('mini-navbar') || angular.element('body').hasClass('body-small')) {
          // Hide menu in order to smoothly turn on when maximize menu
          angular.element('#side-menu').hide();
          // For smoothly turn on menu
          $timeout(function() {
            angular.element('#side-menu').fadeIn(400);
          }, 200);
        } else {
          // Remove all inline style from jquery fadeIn function to reset menu state
          angular.element('#side-menu').removeAttr('style');
        }
      };
    }

  }

})();
(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('sideNavigation', sideNavigation);

  /** @ngInject */
  function sideNavigation($timeout) {
    var directive = {
      restrict: 'A',
      link: sideNavigation
    };
    return directive;

    /** @ngInject */
    function sideNavigation(scope, element) {
      scope.$watch('authentication.user', function() {
        $timeout(function() {
          element.metisMenu();
        });
      });
    }

  }

})();
(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('iboxTools', iboxTools);

  /** @ngInject */
  function iboxTools($timeout) {
    var directive = {
      restrict: 'A',
      scope: true,
      templateUrl: 'components/inspinia/directives/ibox-tools/iboxTools.html',
      controller: iboxToolsController
    };
    return directive;

    /** @ngInject */
    function iboxToolsController($scope, $element) {
      // Function for collapse ibox
      $scope.showhide = function() {
        var ibox = $element.closest('div.ibox');
        var icon = $element.find('i:first');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        // Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        $timeout(function() {
          ibox.resize();
          ibox.find('[id^=map-]').resize();
        }, 50);
      };
      // Function for close ibox
      $scope.closebox = function() {
        var ibox = $element.closest('div.ibox');
        ibox.remove();
      }
    }
  }
})();
(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('icheck', icheck);

  /** @ngInject */
  function icheck($timeout) {
    var directive = {
      restrict: 'A',
      require: 'ngModel',
      link: iCheckLink
    };
    return directive;

    /** @ngInject */
    function iCheckLink($scope, element, $attrs, ngModel) {
      return $timeout(function() {
        var value;
        value = $attrs['value'];

        $scope.$watch($attrs['ngModel'], function(newValue) {
          $(element).iCheck('update');
        })

        return $(element).iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green'

        }).on('ifChanged', function(event) {
          if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
            $scope.$apply(function() {
              return ngModel.$setViewValue(event.target.checked);
            });
          }
          if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
            return $scope.$apply(function() {
              return ngModel.$setViewValue(value);
            });
          }
        });
      });
    }
  }
})();
angular.module("zpUtils").run(["$templateCache", function($templateCache) {$templateCache.put("components/pagination/pagination.html","<nav ng-show=\"vm.pagesCount > 1\">\n  <ul class=\"pagination\">\n    <li ng-show=\"vm.currentPage > 1\">\n      <a ng-click=\"vm.previousPage()\">\n        <span>&laquo;</span>\n      </a>\n    </li>\n\n    <li ng-repeat=\"page in vm.pages\" ng-class=\"{active: page == vm.currentPage}\"><a ng-click=\"vm.showPage(page)\">{{page}}</a></li>\n    <li ng-show=\"vm.currentPage != vm.pagesCount\">\n      <a ng-click=\"vm.nextPage()\">\n        <span>&raquo;</span>\n      </a>\n    </li>\n  </ul>\n</nav>\n");
$templateCache.put("components/zp-breadcrumb/zpBreadcrumb.html","<ol id=\"breadcrumb\" class=\"breadcrumb\">\n  <li ng-repeat=\"b in breadcrumb.path\">\n    <a ui-sref=\"{{b.state}}\" translate>{{b.name}}</a>\n  </li>\n  <li>\n    <span translate class=\"current\">{{breadcrumb.currentState}}</span>\n  </li>\n</ol>");
$templateCache.put("components/zp-messages/zpMessages.content.html","<small class=\"text-danger\" ng-message=\"required\"><span translate>_formErrors.required</span></small>\n<small class=\"text-danger\" ng-message=\"minlength\"><span translate>_formErrors.minlength</span></small>\n<small class=\"text-danger\" ng-message=\"maxlength\"><span translate>_formErrors.maxlength</span></small>\n<small class=\"text-danger\" ng-message=\"sameAs\"><span translate>_formErrors.sameAs</span></small>\n<small class=\"text-danger\" ng-message=\"pattern\"><span translate>_formErrors.pattern</span></small>\n<small class=\"text-danger\" ng-message=\"email\"><span translate>_formErrors.email</span></small>\n<small class=\"text-danger\" ng-message=\"min\"><span translate>_formErrors.min</span></small>\n<small class=\"text-danger\" ng-message=\"max\"><span translate>_formErrors.max</span></small>\n");
$templateCache.put("components/zp-messages/zpMessages.html","<div ng-show=\"showTips(name)\" class=\"message-box\">\n  <div class=\"zp-input-message\" ng-messages=\"messagesContext(name)\" ng-show=\"showTips(name)\">\n    <div ng-messages-include=\"components/zp-messages/zpMessages.content.html\"></div>\n  </div>\n</div>");
$templateCache.put("components/inspinia/directives/ibox-tools/iboxTools.html","<div class=\"ibox-tools dropdown\" dropdown>\n  <a ng-click=\"showhide()\"> <i class=\"fa fa-chevron-up\"></i></a>\n<!-- <a class=\"dropdown-toggle\" href dropdown-toggle>\n  <i class=\"fa fa-wrench\"></i>\n</a>\n -->\n  <!--   <ul class=\"dropdown-menu dropdown-user\">\n    <li><a href>Config option 1</a>\n    </li>\n    <li><a href>Config option 2</a>\n    </li>\n  </ul> -->\n  <a ng-click=\"closebox()\"><i class=\"fa fa-times\"></i></a>\n</div>\n");}]);