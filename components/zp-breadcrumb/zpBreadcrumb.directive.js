(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('zpBreadcrumb', zpBreadcrumb);

  /** @ngInject */
  function zpBreadcrumb() {
    var directive = {
      restrict: 'E',
      templateUrl: 'components/zp-breadcrumb/zpBreadcrumb.html',
      controller: zpBreadcrumbController,
      controllerAs: 'breadcrumb',
      scope: {}

    };

    return directive;

    /** @ngInject */
    function zpBreadcrumbController($rootScope, $state) {
      var breadcrumb = this;

      function updateBreadcrumb(toState) {
        breadcrumb.path = [];
        toState.data = toState.data || {};
        var statePath = toState.data.breadcrumbPath || [];
        for (var x = 0; x < statePath.length; x++) {
          var stateItem = $state.get(statePath[x]);
          breadcrumb.path.push({
            name: stateItem.data.pageTitle,
            state: stateItem.name
          });
        }
        breadcrumb.currentState = toState.data.pageTitle || "Zaszczepiamy Pasje";
      }

      updateBreadcrumb($state.current);
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
        updateBreadcrumb(toState);
      })

    }

  }

})();