(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('paging', paging);

  /** @ngInject */
  function paging() {
    return {
      restrict: 'E',
      templateUrl: 'components/pagination/pagination.html',
      controller: PaginationController,
      controllerAs: 'vm',
      scope: {
        loadingFunction: "&",
        entities: "=",
        onValueChange: "="
      }
    };

    /** @ngInject */
    function PaginationController($scope) {
      var vm = this;

      function range(max) {
        var x = [];
        for (var i = 1; i <= max; i++) {
          x.push(i);
        }
        return x
      }

      function loadPage(page, force) {
        var force = force || false;
        if (page == vm.currentPage && !force) return;
        $scope.loadingFunction()(Number(page)).then(function(response) {
          vm.pagesCount = response.pagesCount || response.totalPages;
          $scope.entities = response.entities;
          vm.pages = range(vm.pagesCount);
          vm.currentPage = page;
        })
      }

      function nextPage() {
        if (vm.currentPage < vm.pagesCount) {
          loadPage(vm.currentPage + 1)
        }
      }

      function previousPage() {
        if (vm.currentPage > 1) {
          loadPage(vm.currentPage - 1)
        }
      }

      loadPage(1);

      vm.showPage = loadPage;
      vm.nextPage = nextPage;
      vm.previousPage = previousPage;


      $scope.$watch('onValueChange', function() {
        loadPage(1, true)
      });

    }
  }

})();
