(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('zpMessages', zpMessages);

  /** @ngInject */
  function zpMessages() {
    var directive = {
      restrict: 'A',
      require: '^?form',
      scope: {
        name: "@messageName"
      },

      templateUrl: 'components/zp-messages/zpMessages.html',

      link: zpMessagesLink
    };

    return directive;

    /** @ngInject */
    function zpMessagesLink(scope, element, attr, form) {
      //scope.name = attr.messageName;
      scope.messagesContext = messagesContext;
      scope.showTips = showTips;

      function messagesContext(fieldName) {
        return form[fieldName].$error;
      }

      function showTips(fieldName) {
        return form.$submitted && Object.keys(form[fieldName].$error).length;
      }
    }
  }
})();