(function() {
  'use strict';

  angular
    .module('zpUtils')
    .factory('zpForms', zpForms);

  /** @ngInject */
  /*TODO: it has to be more professional*/
  function zpForms() {
    function unsubmitForm(form) {
      form.$setPristine();
      form.$setUntouched();
    }

    function clearInputs(form, exp) {
      var patt = new RegExp(exp);
      for (var i in form) {
        if (!patt.test(i)) {
          continue;
        }
        var field = form[i];
        field.$setPristine();
        field.$setUntouched();
      }
    }
    return {
      clearForm: function(form) {
        var formHtml = document.querySelector('form[name="' + form.$name + '"]');
        form.$setPristine();
        form.$setUntouched();
        for (var i in form) {
          if (i.indexOf('$') !== -1) {
            continue;
          }
          var field = form[i];
          var fieldHtml = formHtml.querySelector('[name="' + i + '"]');

          fieldHtml.value = "";
          field.$setUntouched();
        }
        return {};
      },
      resetInputsAndForm: function(form, exp) {
        clearInputs(form, exp);
        unsubmitForm(form);
      },
      unsubmitForm: function(form) {
        if (typeof form === 'undefined') {
          return;
        }
        unsubmitForm(form);
      }
    };
  }

})();