(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('iboxTools', iboxTools);

  /** @ngInject */
  function iboxTools($timeout) {
    var directive = {
      restrict: 'A',
      scope: true,
      templateUrl: 'components/inspinia/directives/ibox-tools/iboxTools.html',
      controller: iboxToolsController
    };
    return directive;

    /** @ngInject */
    function iboxToolsController($scope, $element) {
      // Function for collapse ibox
      $scope.showhide = function() {
        var ibox = $element.closest('div.ibox');
        var icon = $element.find('i:first');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        // Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        $timeout(function() {
          ibox.resize();
          ibox.find('[id^=map-]').resize();
        }, 50);
      };
      // Function for close ibox
      $scope.closebox = function() {
        var ibox = $element.closest('div.ibox');
        ibox.remove();
      }
    }
  }
})();