(function() {
  'use strict';

  angular
    .module('zpUtils')
    .directive('sideNavigation', sideNavigation);

  /** @ngInject */
  function sideNavigation($timeout) {
    var directive = {
      restrict: 'A',
      link: sideNavigation
    };
    return directive;

    /** @ngInject */
    function sideNavigation(scope, element) {
      scope.$watch('authentication.user', function() {
        $timeout(function() {
          element.metisMenu();
        });
      });
    }

  }

})();