var gulp = require('gulp');
var uglify = require('gulp-uglifyjs');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var clean = require('gulp-clean');
var templateCache = require('gulp-angular-templatecache');
var streamqueue = require('streamqueue');

gulp.task('dist', function() {

  var styleFiles = [
    "stylesheets/inspinia/scss/font.scss",
    "stylesheets/inspinia/scss/partials/_variables.scss",
    "stylesheets/inspinia/scss/partials/_mixins.scss",
    "stylesheets/inspinia/scss/partials/_typography.scss",
    "stylesheets/inspinia/scss/partials/_navigation.scss",
    "stylesheets/inspinia/scss/partials/_top_navigation.scss",
    "stylesheets/inspinia/scss/partials/_buttons.scss",
    "stylesheets/inspinia/scss/partials/_badgets_labels.scss",
    "stylesheets/inspinia/scss/partials/_elements.scss",
    "stylesheets/inspinia/scss/partials/_sidebar.scss",
    "stylesheets/inspinia/scss/partials/_base.scss",
    "stylesheets/inspinia/scss/partials/_pages.scss",
    "stylesheets/inspinia/scss/partials/_chat.scss",
    "stylesheets/inspinia/scss/partials/_metismenu.scss",
    "stylesheets/inspinia/scss/partials/_spinners.scss",
    "stylesheets/inspinia/scss/partials/_landing.scss",
    "stylesheets/inspinia/scss/partials/_rtl.scss",
    "stylesheets/inspinia/scss/partials/_theme-config.scss",
    "stylesheets/inspinia/scss/partials/_skins.scss",
    "stylesheets/inspinia/scss/partials/_md-skin.scss",
    "stylesheets/inspinia/scss/partials/_media.scss",
    "stylesheets/inspinia/scss/partials/_custom.scss",
    "stylesheets/zp/mixins.scss",
    "stylesheets/zp/global.scss",
    'components/**/*.scss'
  ]


  streamqueue({ objectMode: true },
      // gulp.src(['bower_components/bootstrap-css/css/bootstrap.css']),
      gulp.src(styleFiles)
      .pipe(concat('dist.scss'))
      //.pipe(sass())
    )
    //.pipe(concat('dist.css'))
    .pipe(gulp.dest('./dist'));


  streamqueue({ objectMode: true },
      gulp.src(['components/**/*.js'])
      .pipe(concat('temp.js')),
      gulp.src('components/**/*.html')
      .pipe(templateCache({ module: 'zpUtils', root: 'components' })))
    .pipe(concat('dist.js'))
    //.pipe(uglify())
    .pipe(gulp.dest('./dist'));

});

gulp.task('build', ['dist']);